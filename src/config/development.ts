import IConfiguration from "../types/IConfiguration"

export class config implements IConfiguration {
  port: number;

  constructor() {
    this.port = 3993
  }
}