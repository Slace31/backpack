/**
 * Interface that define the environment configuration
 */

export default interface IConfiguration {
  port: number
}