import { RequestHandler } from "express";
import { HttpMethod } from "./HttpMethod";

export default interface IBackpackController {
  url: string

  method?: HttpMethod
  handler: RequestHandler
}