import * as express from "express"
import IConfiguration from "./types/IConfiguration"
import * as path from "path"
import IBackpackController from "./types/IBackpackController";
import { HttpMethod } from "./types";

const { NODE_ENV = 'development' } = process.env
const Configuration: IConfiguration = new (require(`./config/${NODE_ENV}`).config)()
const apiRoutes: IBackpackController[] = require('./routes').default
const apiLocation = `http://${require('my-local-ip')()}:${Configuration.port}`

const app = express()

app.listen(Configuration.port, () => {
  console.log(`Server running on ${apiLocation}`)
})

if (NODE_ENV === 'production') {
  const appDir = path.resolve(__dirname, 'views')

  app.use(express.static(appDir));

  // app.all("/api", apiRoutes) 
  app.get('*', function (req, res) {
    res.sendfile(path.resolve(appDir, "index.html"));
  });
} else {
  console.log('Custom API Routes:')
  apiRoutes.map(route => {
    console.log(`${route.method.toUpperCase()} ${apiLocation}${route.url}`)
    switch (route.method) {
      case HttpMethod.GET:
        return app.get(route.url, route.handler)
      case HttpMethod.POST:
        return app.post(route.url, route.handler)
      case HttpMethod.DELETE:
        return app.delete(route.url, route.handler)
      case HttpMethod.PUT:
        return app.put(route.url, route.handler)
      case HttpMethod.PATCH:
        return app.patch(route.url, route.handler)
      default:
        return app.all(route.url, route.handler)
    }
  })

  app.get("/", (req, res) => {
    res.redirect(301, 'http://localhost:3000')
  })

  // app.all("/api", apiRoutes)
}