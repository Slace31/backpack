import { RequestHandler } from "express"
import IBackpackController from "../types/IBackpackController"
import { HttpMethod } from "../types";

class HelloWorld implements IBackpackController {
  url: string = "/hello-world"
  method: HttpMethod = HttpMethod.GET

  handler: RequestHandler = (req, res, next) => {
    res.send('Hello World !')
  }
}

export default HelloWorld