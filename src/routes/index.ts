import IBackpackController from "../types/IBackpackController"

import helloWord from './helloWord'

const apiControllers: IBackpackController[] = [
  new helloWord(),
]
export default apiControllers