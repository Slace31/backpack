module.exports = {
  expressLog: data => console.log("\x1b[36m%s\x1b[0m", "[express] " + data),
  reactLog: data => console.log("\x1b[33m%s\x1b[0m", "[react] " + data),
  errorLog: data => console.error("\x1b[31m", data),
  imageLog: (imagePath = './assets/backpack_logo.jpg') => {
    const art = require('ascii-art')

    var image = new art.Image({
      filepath: imagePath,
      alphabet: 'variant4'
    });

    image.write(function (err, rendered) {
      console.log(rendered);
    })
  }
}