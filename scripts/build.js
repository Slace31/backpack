const minimist = require('minimist')
const path = require('path')
const { renameSync, mkdirSync, writeFileSync } = require('fs')
const { reactLog, expressLog, errorLog } = require('../utils/log')
const { spawn } = require('child_process')
const rimraf = require('rimraf')

process.chdir(path.resolve(__dirname, '..'))

const argv = minimist(process.argv.slice(2))
const distFolder = argv.d ? path.normalize(argv.d) : './dist'

rimraf(distFolder, () => {
  mkdirSync(distFolder)

  reactLog("Starting build")
  const reactBuild = spawn(/^win/.test(process.platform) ? 'npm.cmd' : 'npm', ["run", "react:build"], { cwd: __dirname })
  reactBuild.stdout.on("data", reactLog)
  reactBuild.stderr.on("data", reactLog)

  reactBuild.on('close', code => {
    if (code !== 0) {
      errorLog("React build failed, stoping building script")
      return process.exit(1)
    } else {
      reactLog("build ended")

      reactLog("Moving react dist folder to dist views folder")
      renameSync('./views/build', path.resolve(distFolder, 'views'))
      reactLog("Copy ended")
    }
  })

  expressLog("Starting build")
  const serverBuild = spawn(/^win/.test(process.platform) ? 'npm.cmd' : 'npm', ["run", "server:build"], { cwd: __dirname })
  serverBuild.stdout.on("data", expressLog)
  serverBuild.stderr.on("data", expressLog)

  serverBuild.on('close', code => {
    if (code !== 0) {
      errorLog("Express build failed, stoping building script")
      return process.exit(1)
    } else {
      expressLog("Build ended")
      expressLog("Making the package.json file")

      const packageJson = require('../package.json')
      const distPackageJson = {
        name: packageJson.name,
        version: packageJson.version,
        description: packageJson.description,
        main: "server.js",
        scripts: {
          start: "NODE_ENV=production node server.js"
        },
        author: packageJson.author,
        license: packageJson.license,
        dependencies: packageJson.dependencies
      }

      writeFileSync(path.resolve(distFolder, 'package.json'), JSON.stringify(distPackageJson, null, 2), 'utf-8')
      expressLog("package.json file created")
    }
  })

  const closeProcesses = () => reactBuild.kill(0) && serverBuild.kill(0)

  process.on("beforeExit", closeProcesses)
})