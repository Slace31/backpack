const { spawn } = require("child_process")
const { reactLog, expressLog, imageLog } = require('../utils/log')

imageLog()

// Start Express
const express = spawn(/^win/.test(process.platform) ? 'npm.cmd' : 'npm', ["run", "server:dev"], { cwd: __dirname, env: process.env })
express.on("error", err => console.log(err))
express.stdout.on("data", expressLog)
express.stderr.on("data", expressLog)

// Start React
const react = spawn(/^win/.test(process.platform) ? 'npm.cmd' : 'npm', ["run", "react:start"], { cwd: __dirname, env: process.env })
react.stdout.on("data", reactLog)
react.stderr.on("data", reactLog)

const closeProcesses = () => express.kill(0) && react.kill(0)

process.on("beforeExit", closeProcesses)
